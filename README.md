Our facilities and services are designed to assist in routine preventative care for young healthy pets, early detection and treatment of disease as your pet ages, and medical care as necessary during his or her entire life. We also specialize in Exotic Care.

Address: N48 W14850 W. Lisbon Road, Menomonee Falls, WI 53051, USA

Phone: 262-781-5277
